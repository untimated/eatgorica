<?php
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    session_start();
    include("connectDB.php");
?>
<?php
    $phoneNumberErrorMessage = "";
    $phoneNumberPattern = "/0[0-9]{3}-[0-9]{4}-[0-9]{4}/";
    $divInputTextState = "form-group";

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $phoneNumber = $_POST["phoneNum"];
        if(empty($phoneNumber)) {

            $phoneNumberErrorMessage = "Mohon Diisi";
            $divInputTextState = "form-group has-error";
        }else{
            if(preg_match($phoneNumberPattern,$phoneNumber)==1){
                $query = "CALL findUserByPhoneNumber('$phoneNumber');";
                $result = mysqli_query($conn, $query);
                if($result === FALSE) {
                    die(mysql_error());
                }
                while($resultRow = mysqli_fetch_row($result)){
                    $rowCount = $resultRow[0];
                }

                mysqli_free_result($result);
                mysqli_next_result($conn);
                if($rowCount != 0) {
                    $query = "CALL getCustomer('$phoneNumber')";
                    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));

                    $row = mysqli_fetch_row($result);
                    $name = $row[0];
                    $email = $row[1];
                    $_SESSION['user_name'] = $name;
                    $_SESSION['user_email'] = $email;
                    $_SESSION['user_phone'] = $phoneNumber;
                    mysqli_close($conn);
                    header('Location:dashboard.php');

                }else {
                    $_SESSION['user_phone'] = $phoneNumber;
                    mysqli_close($conn);

                    header('Location:register.php');
                }
            }else{
                $phoneNumberErrorMessage = "Input Salah";
                $divInputTextState = "form-group has-error";
            }
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">
    

    <title>gorica</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/ionicons.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/jquery-1.6.4.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- smooth scrolling script -->
    <script type="text/javascript">
        $(window).load(function() {
            $('a').click(function() {
                $('html, body').animate({
                    scrollTop: $($(this).attr('href')).offset().top
                }, 500);
                return false;
            });
        })
    </script>

        <!-- end smooth scrolling script -->

         <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
         <!--[if lt IE 9]>
         < script src = "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" >
    </script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div id="h">
        <div class="logo">gorica</div>
        <div class="social hidden-xs">

            <a href="#"><i class="ion-social-instagram"></i></a>
            <a href="#"><i class="ion-social-facebook"></i></a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 centered">
                    <h1>Selamat Datang<br/>Daftar sekarang dan pesan gorica.</h1>
                    <div class="mtb">
                        
                        <a href="#g" class='btn btn-conf btn-green'>Pesan</a>

                    </div>
                    <!--/mt-->
                    <h6>30 DAY FREE TRIAL - NO CREDIT CARD NEEDED.</h6>
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!-- /H -->

    <div class="container ptb">
        <div class="row">
            <div class="col-md-6">
                <h2>Apa itu gorica?</h2>
                <p class="mt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

            </div>
            <div class="col-md-6">
                <img src="assets/img/abon%20kemasan.jpg" class="img-responsive mt" alt="">
            </div>
        </div>
        <!--/row-->
    </div>
    <!--/container-->

    
    <!--FORM TO PROCESS PHONENUMBER-->
    <div id="g">
        <div class="container">
            <div class="row">
                <h2><center>Pesan Sekarang</center></h2>
                <br/>
                
                <div class = "container">
                     <div class="col-sm-12 col-md-12">      
                         <form class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>#g" method="POST">
                             <div class = "row">
                                <div class="<?php echo $divInputTextState; ?>">
                                    <div class="col-sm-12">
                                        <?php echo $phoneNumberErrorMessage;?>
                                         <input type="text" class="form-control" name="phoneNum" placeholder="Nomor Handphone (contoh : 0878-XXXX-XXXX)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <input type="submit" class="btn btn-default btn-lg btn-block" value="Lanjut"></input>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>  
                
            
            <!--/row-->
        </div>
    </div>
    <!--/g-->

    <div id="green">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 centered">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <h3>I enjoyed so much the last edition of Landing Sumo, that I bought the tickets for the new one edition of the event the first day.</h3>
                                <h5><tgr>DAVID JHONSON</tgr></h5>
                            </div>
                            <div class="item">
                                <h3>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h3>
                                <h5><tgr>MARK LAWRENCE</tgr></h5>
                            </div>
                            <div class="item">
                                <h3>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration, by injected humour.</h3>
                                <h5><tgr>LISA SMITH</tgr></h5>
                            </div>
                        </div>
                    </div>
                    <!--/Carousel-->

                </div>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!--/green-->

    <div id="f">
        <div class="container">
            <div class="row centered">
                <h2>You Can Contact Us</h2>
                <h5>HELLO@LANDINGSUMO.COM</h5>

                <p class="mt">
                    <a href="#"><i class="ion-social-twitter"></i></a>
                    <a href="#"><i class="ion-social-dribbble"></i></a>
                    <a href="#"><i class="ion-social-instagram"></i></a>
                    <a href="#"><i class="ion-social-facebook"></i></a>
                    <a href="#"><i class="ion-social-pinterest"></i></a>
                    <a href="#"><i class="ion-social-tumblr"></i></a>
                </p>
                <h6 class="mt">COPYRIGHT 2014 - LANDING SUMO</h6>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!--/F-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
</body>

</html>
