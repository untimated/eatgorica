<?php

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    
    session_start();

    if(!isset($_SESSION['user_phone']))
    {
        header("Location:index.php");
    }

    include_once('connectDB.php');

    $query = "SELECT `id` FROM `Customer` WHERE `phone` = \"{$_SESSION['user_phone']}\"";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));

    $row = mysqli_fetch_row($result);
    $customerId = $row[0];
    $_SESSION['Customer_id'] = $customerId;

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>gorica</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/ionicons.min.css" rel="stylesheet">
    <link href="assets/css/style.css" type="text/css" rel="stylesheet">
    


    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/mmenu_js/jquery.mmenu.min.js" type="text/javascript"></script>

    <link href="assets/css/mmenu_css/jquery.mmenu.css" rel="stylesheet">
    <link href="assets/css/mmenu_css/extensions/jquery.mmenu.effects.css" type="text/css" rel="stylesheet">
    <link href="assets/css/mmenu_css/extensions/jquery.mmenu.pageshadow.css" type="text/css" rel="stylesheet">

    <link href="assets/css/mmenu_css/extensions/jquery.mmenu.themes.css" type="text/css" rel="stylesheet">

    <link href="assets/css/mmenu_css/extensions/jquery.mmenu.positioning.css" type="text/css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    

    
    <script type="text/javascript">
       $(document).ready(function() {
          $("#menu").mmenu({
             // options
             extensions: ["theme-dark","pageshadow"],

             offCanvas: {
                position: "right"
                 
             }
          });

          
       });
    </script>
    
    <script>

        function validateOrderForm(){
            var alamat = document.getElementsByName("choose_address")[0].value;
            if(alamat.length === 0 || !alamat.trim()){
                document.getElementById("choose_address_div").className += " has-error";
                document.getElementById("modal_info_error").innerHTML = "Form pemesanan belum terisi dengan benar"
                return false;
            }else  {
                return true;
            }
        }

    </script>

    
    
    <style>
        p.welcome {
            font-size: 26px;
        }

        p.subwelcome {
            font-size: 16px;
        }

    </style>
</head>

<body>
    
<nav id="menu">
   <ul>
     <li class="Selected">
            <ul class="Vertical">
                <li class="Selected"><a href="dashboard.php">Pesan Baru</a></li>
                <li><a href="myOrder.php">Pesanan Saya</a></li>
            </ul>
     </li>
   </ul>
</nav>
    


    
    <div id="headLogin">
        <div class="logo">gorica</div>
        
        <div class="social">
            <a href="#menu"><i><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></i></a>
        </div>
        
        <p class="welcome">hi, <?php echo $_SESSION['user_name']; ?></p>
        <p class="subwelcome"><?php echo $_SESSION['user_email']; ?></p>
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div id="g2">
                            <h2><center>Form Pemesanan</center></h2>
                            <br/><br/>
                            <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                            
                                            <!--Order FORM-->
                                            <div style="padding:10px">
                                            <form class="form-horizontal" onsubmit="return validateOrderForm()" action="process_order.php" method="POST">
                                                
                                                <div class="row">                                                   
                                                    <input class="form-control" id="disabledInput" type="text" placeholder="<?php echo $_SESSION['user_name']?>" readonly>                                                  
                                                </div>
                                                
                                                <div class="row">                                                   
                                                    <input class="form-control" id="disabledInput" type="text" placeholder="<?php echo $_SESSION['user_email']?>" readonly>                                                   
                                                </div>

                                                <div class = "row">
                                                    
                                                    <b>Produk</b>
                                                    <select id="choose_product" name="choose_product" class="form-control">
                                                        <?php
                                                        
                                                        $query = "SELECT `id`,`name` FROM `Product`";
                                                        $result = mysqli_query($conn,$query);
                                                        while($row = mysqli_fetch_assoc($result)){
                                                            $productName = $row["name"];
                                                        echo "<option value='$productName'>".$productName."</option>";
                                                        }
                                                        mysqli_free_result($result);
                                                        //mysqli_close($conn);
                                                        ?>
                                                        
                                                    </select>
                                                    
                                                </div>
                                                
                                                <br/>
                                                <br/>
                                                <b>Jumlah</b>
                                                <div class="row">
                                                    
                                                    <div class="col-xs-12 col-sm-12 col-md-9" style="padding-left : 0px; padding-right : 0px ">
                                                        <input name="quantity" class="form-control" type="text" placeholder="0"  value="0" style="text-align:center" readonly/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-1"></div>
                                                    <div class="col-xs-12 col-sm-12 col-md-1 " style="padding-left : 5px; padding-right : 5px; padding-bottom:5px">
                                                        <button name="plus" type="button" class="btn btn-success btn" style="height:100%;width:100%;" onclick="addQuantity();">
                                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                                        </button>
                                                    </div>
                                                    
                                                    <div class="col-xs-12 col-sm-12 col-md-1 " style="padding-left : 5px; padding-right : 5px">
                                                        <button type="button" name="minus" class="btn btn-warning btn" style="height:100%;width:100%;" onclick="decreaseQuantity()">
                                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                                        </button>
                                                    </div>
                                                </div>
                                                
                                                <br/><br/><br/>
                                                <div class = "row">
                                                    
                                                    <b>Kota Tujuan</b>
                                                    <select name="choose_city" class="form-control">
                                                        <?php
                                                        //include_once("connectDB.php");
                                                        $query = "SELECT `destination` FROM `tariffjne`";
                                                        $result = mysqli_query($conn,$query);
                                                        while($row = mysqli_fetch_assoc($result)){
                                                        echo "<option value={$row["destination"]}>".$row["destination"]."</option>";
                                                        }
                                                        mysqli_free_result($result);
                                                        //mysqli_close($conn);?>
                                                    </select>
                                                    
                                                </div>
                                                <br/>
                                                <div class="row" id="choose_address_div">
                                                    
                                                    <input type="text" class="form-control" name="choose_address" placeholder="Alamat Pengiriman">
                                                    
                                                </div>
                                                
                                                <br/><br/>
                                                
                                                <!--Modal Confirmation-->
                                                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div id="modal_info" class="modal-body">
                                                                Pastikan informasi pesanan anda sudah benar,
                                                                Lanjut ?
                                                                <p id="modal_info_error" style="color:#cf2a2a"></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <script>
                                                                function clearModalErrorMessage(){
                                                                document.getElementById("modal_info_error").innerHTML = null;
                                                                }
                                                                </script>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="clearModalErrorMessage()">Batal</button>
                                                                <input type="submit" class="btn btn-primary" value="Lanjut"></input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <!-- ORDER SUMMARY -->
                                            
                                                <div id="panel_info" class="panel panel-default">
                                                    <div class="panel-heading">
                                                        
                                                        <button type="button" class="btn btn-default" aria-label="Right Align" onclick = "calculateTariff()">
                                                        <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                                                        </button>
                                                        Ringkasan
                                                    </div>
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left">Produk</td>
                                                                <td><div id="out_product"></div></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Jumlah</td>
                                                                <td><div id="out_amount"></div></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Harga Per Item</td>
                                                                <td><?php echo"Rp. 35000" ?></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Kota Tujuan</td>
                                                                <td><div id="out_destination"></div></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">Logistik</td>
                                                                <td>JNE</td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Ongkos Per KG</td>
                                                                <td><div id="out_pricePerKg"></div></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Berat Pengiriman</td>
                                                                <td><div id="out_weight"></div></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td align="left">Biaya Pengiriman</td>
                                                                <td><div id="out_shippingCost"></div></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="danger" align="left">Total</td>
                                                                <td class="danger"><div id="out_total"></div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            
                                        </div>    
                                    </div>

                                    <div class = "row">
                                            <div class="col-sm-offset-4 col-sm-4">
                                                <button type="button" class="btn btn-default" data-toggle="modal" data-target=".bs-example-modal-sm">Lanjut</button>
                                            </div>
                                       </div>

                                    <!--/row-->
                            </div>
                            
                        </div>
                            <!--/g2-->
                        </div>
                        <br/>
                           
                        
                        <div class="container ptb">
                            <div class="row">
                                <div class="col-md-6">
                                    <h2>Promo Gorica</h2>
                                    <p class="mt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                                </div>
                                <div class="col-md-6">
                                    <img src="assets/img/abon%20kemasan.jpg" class="img-responsive mt" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                        
    <div id="f">
        <div class="container">
            <div class="row centered">
                <h2>You Can Contact Us</h2>
                <h5>HELLO@LANDINGSUMO.COM</h5>

                <p class="mt">
                    <a href="#"><i class="ion-social-twitter"></i></a>
                    <a href="#"><i class="ion-social-dribbble"></i></a>
                    <a href="#"><i class="ion-social-instagram"></i></a>
                    <a href="#"><i class="ion-social-facebook"></i></a>
                    <a href="#"><i class="ion-social-pinterest"></i></a>
                    <a href="#"><i class="ion-social-tumblr"></i></a>
                </p>
                <h6 class="mt">COPYRIGHT 2014 - LANDING SUMO</h6>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>
    <!--/F-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
    <script src="assets/js/calculation.js"></script>
</body>
</html>
