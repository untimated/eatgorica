<?php session_start(); 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>gorica</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/ionicons.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/jquery-1.6.4.js"></script>

    
    <script type="text/javascript">
         function validateForm() {
                var inputName = document.loginForm.name.value;
                var inputEmail = document.loginForm.email.value;
                var emailPattern = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
                var namePattern = /^[a-zA-Z ]+$/;
                var nameIsCorrect = false;
                var emailIsCorrect = false;
             
                if(!inputEmail){        
                    document.getElementById("emailWarning").innerHTML = "Email Kosong";
                    emailIsCorrect = false;
                }else {
                    if(emailPattern.test(inputEmail)){
                        document.getElementById("emailWarning").innerHTML = " ";
                        emailIsCorrect = true;
                    }else {emailIsCorrect = false; document.getElementById("emailWarning").innerHTML = "Email Salah"};
                }
             
                if(!inputName){        
                    document.getElementById("nameWarning").innerHTML = "Nama Kosong";
                    nameIsCorrect = false;
                }else {
                    if(namePattern.test(inputName)){
                        document.getElementById("nameWarning").innerHTML = " ";
                        nameIsCorrect = true;
                    }else {nameIsCorrect = false; document.getElementById("nameWarning").innerHTML = "Nama Salah";}
                }

                return nameIsCorrect && emailIsCorrect;
            }
             
    </script>


    <!-- end smooth scrolling script -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
         < script src = "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" >
    </script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="headLogin">
        <div class="logo">gorica</div>
        
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Anda belum terdaftar, silahkan masukan nama dan email</h3>
                                </div>
                                <div class="panel-body">

                                    <?php if(isset($_SESSION['user_phone'])){
                                        
                                        echo '<form accept-charset="UTF-8" name="loginForm" action="registerRedirect.php" onsubmit="return validateForm()" method="POST">'; 
                                        echo '<fieldset>';
                                        echo '<p id="emailWarning" style="color:red;font-weight:bold;"></p>';
                                        echo '<div class="form-group"><input class="form-control" placeholder="E-mail" name="email" type="text"></div>';
                                        echo '<p id="nameWarning" style="color:red;font-weight:bold;"></p>';
                                        echo '<div class="form-group"> <input class="form-control" placeholder="Nama Lengkap" name="name" type="text"></div>'; 
                                        
                                        echo '</br><input class="btn btn-lg btn-success btn-block" type="submit" value="Daftar">'; 
                                        
                                        echo '</fieldset></form>'; 
                                        } else { 
                                            echo 'Anda Belum Mengisi Nomor Telepon Anda Silahkan Kembali Ke halaman Utama <br/>'; echo '<a href ="index.php">menu utama</a>';
                                        }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div>






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
</body>

</html>
