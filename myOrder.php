<?php

    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    session_start();

    if(!isset($_SESSION['user_phone']))
    {
        header("Location:index.php");
    }
?>

    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="assets/img/favicon.ico">

        <title>gorica</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="assets/css/flat-table.css" rel="stylesheet">
        <link href="assets/css/ionicons.min.css" rel="stylesheet">
        <link href="assets/css/style.css" type="text/css" rel="stylesheet">



        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="assets/js/mmenu_js/jquery.mmenu.min.js" type="text/javascript"></script>

        <link href="assets/css/mmenu_css/jquery.mmenu.css" rel="stylesheet">
        <link href="assets/css/mmenu_css/extensions/jquery.mmenu.effects.css" type="text/css" rel="stylesheet">
        <link href="assets/css/mmenu_css/extensions/jquery.mmenu.pageshadow.css" type="text/css" rel="stylesheet">
        <link href="assets/css/mmenu_css/extensions/jquery.mmenu.themes.css" type="text/css" rel="stylesheet">
        <link href="assets/css/mmenu_css/extensions/jquery.mmenu.positioning.css" type="text/css" rel="stylesheet">
        <link href ="assets/js/stacktable/stacktable.css" rel = "stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
        
        <script src="assets/js/stacktable/stacktable.js"></script>


        <script type="text/javascript">
            $(document).ready(function () {

                $("#menu").mmenu({
                    // options
                    extensions: ["theme-dark", "pageshadow"],

                    offCanvas: {
                        position: "right"

                    }
                });


                $('#card-table').cardtable({myClass:'stacktable small-only'});

            });


            Number.prototype.format = function(n, x) {
                var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
            };

            function showOrderModal(id,quantity,price)
            {
                //alert(id,price)
                
                document.getElementById('order_number').innerHTML = id;
                document.getElementById('order_quantity').innerHTML = quantity;
                document.getElementById('order_price').innerHTML = "Rp. " + price.format(2);

                $('#myOrderModal').modal('show');
            }


            function submitPayment()
            {
                $('#myOrderForm').submit();
            }
        </script>



        <style>
            p.welcome {
                font-size: 26px;
            }

            p.subwelcome {
                font-size: 16px;
            }
        </style>
    </head>

    <body>

        <nav id="menu">
            <ul>
                <li class="Selected">
                    <a href="demo.html">Dashboard</a>
                    <ul class="Vertical">
                        <li><a href="dashboard.php">Pesan Baru</a></li>
                        <li class="Selected"><a href="#">Pesanan Saya</a></li>
                    </ul>
                </li>
            </ul>
        </nav>




        <div id="headLogin">
            <div class="logo">gorica</div>

            <div class="social">
                <a href="#menu"><i><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></i></a>
            </div>

            <p class="welcome">hi,
                <?php echo $_SESSION['user_name']; ?>
            </p>
            <p class="subwelcome">
                <?php echo $_SESSION['user_email']; ?>
            </p>
            <div class="container">
                <div class="row">
                    <div class="container">

                        <div class="row">
                            <div id="g2">
                                    <div align=center>
                                        <i style="color:#FF3B30;padding-right:20px"><span class="glyphicon glyphicon-remove" title="Belum Dibayar"></span> Belum Dibayar</i>
                                        <i style="color:#89C40D;padding-right:20px"><span class="glyphicon glyphicon-ok" title="Sudah Dibayar, pesanan anda sedang diproses"></span> Sedang Diproses</i>
                                        <i style="color:#8E5900;padding-right:20px"><span class="glyphicon glyphicon-time" title="Menunggu Konfirmasi Sistem"></span>Memeriksa Pembayaran</i>
                                    </div>

                                <div class="panel panel-default" style="margin-right:10px;margin-left:10px;">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">Pesanan Saya</h2>
                                    </div>



                                    <br/>
                                    <table class="table" id="card-table">
                                      <thead>
                                        <tr>
                                            <th class="st-head-row st-key">Tanggal</th>
                                            <th class= "st-head-row st-key">Alamat Tujuan</th>
                                            <th class="st-head-row st-key">Produk</th>
                                            <th class="st-head-row st-key">Jumlah</th>
                                            <th class="st-head-row st-key">Total</th>
                                            <th class="st-head-row st-key">Status</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                     
                                        <tbody>
                                        

                                            <?php
                                                
                                                include_once('connectDB.php');
                                                $query = "SELECT * FROM `Order` WHERE Customer_id = {$_SESSION['Customer_id']} ORDER BY `orderDate` DESC";


                                                if(isset($_POST['button_select'])){
                                                    //echo $_POST['button_select'];

                                                    $query2 = "UPDATE `Order` SET `paid`= 1 WHERE `id` = {$_POST['button_select']};";
                                                    mysqli_query($conn,$query2) or die(mysqli_error($conn));

                                                    unset($_POST['button_select']);
                                                }

                                                $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
                                                $setStatusIcon = "";
                                                while($row = mysqli_fetch_assoc($result)){
                                                    $urlself = htmlspecialchars($_SERVER["PHP_SELF"]);

                                                    //set icon status logical statement
                                                    if($row['paid'] == 0){
                                                        $setStatusIcon = '<i style="color:#FF3B30"><span class="glyphicon glyphicon-remove" title="Belum Dibayar"></i>';                              
                                                    }else if($row['paid_confirm'] == 1){
                                                        $setStatusIcon = '<i style="color:#89C40D;"><span class="glyphicon glyphicon-ok" title="Sudah Dibayar, pesanan anda sedang diproses"></i>';
                                                    }else if($row['paid'] == 1){
                                                        $setStatusIcon = '<i style="color:#8E5900"><span class="glyphicon glyphicon-time" title="Menunggu Konfirmasi Sistem"></i>';
                                                    }

                                                        echo "<tr class=".($row['paid']==1?'warning':'default').">
                                                        <td>".date('d-m-Y', strtotime($row['orderDate']))."</td>
                                                        <td>{$row['city']}</td>
                                                        <td>Abon Ikan Cakalang</td>
                                                        <td>{$row['quantity']}</td>
                                                        <td>Rp. ".str_replace('.',',',number_format(((int)$row['totalPrice'])))."</td>
                                                        <td>".$setStatusIcon."</td>
                                                        <td>
                                                        <button type=button class='btn btn-info' id='orderButton{$row['id']}' ".($row['paid']==1?'disabled':'')." onclick='showOrderModal({$row['id']},{$row['quantity']},{$row['totalPrice']})'>Bayar</button>
                                                        <form id='myOrderForm' action='{$urlself}' method='post'>
                                                        <input type='hidden' name='button_select' value={$row['id']}></input>
                                                        </form>
                                                        </td></tr>";
                                                }
                                            ?>
                                        
                                       </tbody> 
                                    </table>
                                        
                                    
                                </div>
                            </div>

                            

                            <div class="modal fade" id="myOrderModal">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"><b>Billing Detail</b></h4>
                                  </div>
                                  <div class="modal-body">
                                    

                                    <table class="table">
                                    <tbody>
                                        <tr class="active">
                                            <td colspan="2">Order #<div id="order_number"></div> items</td>
                                            
                                        </tr>
                                        <tr>
                                            <td width=350>Jumlah Barang</td>
                                            <td align=left><div id="order_quantity"></div></td>
                                        </tr>
                                        <tr>
                                            <td width=350>Total</td>
                                            <td align=left><div id="order_price"></div></td>
                                        </tr>
                                    </tbody>
                                    </table>

                                  </div>
                                  <div class="modal-footer">
                                  <p>Apakah pembayaran sudah dilakukan?</p>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Belum</button>
                                    <button type="button" class="btn btn-primary" onclick="submitPayment()">Sudah</button>
                                  </div>
                                </div><!-- /.modal-content -->
                              </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <br/>

        <div id="f">
            <div class="container">
                <div class="row centered">
                    <h2>You Can Contact Us</h2>
                    <h5>HELLO@LANDINGSUMO.COM</h5>

                    <p class="mt">
                        <a href="#"><i class="ion-social-twitter"></i></a>
                        <a href="#"><i class="ion-social-dribbble"></i></a>
                        <a href="#"><i class="ion-social-instagram"></i></a>
                        <a href="#"><i class="ion-social-facebook"></i></a>
                        <a href="#"><i class="ion-social-pinterest"></i></a>
                        <a href="#"><i class="ion-social-tumblr"></i></a>
                    </p>
                    <h6 class="mt">COPYRIGHT 2014 - LANDING SUMO</h6>
                </div>
                <!--/row-->
            </div>
            <!--/container-->
        </div>
        <!--/F-->

        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script>
            
        </script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/retina-1.1.0.js"></script>

    </body>

    </html>
