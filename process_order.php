<?php
    
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    session_start();

    include_once("connectDB.php");
    $amount = $_POST["quantity"];
    $address = $_POST["choose_address"];
    $city = $_POST["choose_city"];
    $phoneNumber = $_SESSION["user_phone"];
    $date = date("Y-m-d H:i:s");
    $product = $_POST["choose_product"];

    $productWeight = 0;
    $totalWeight = 0;
    $productPrice = 0;
    $shippingCost = 0;
    $pricePerKg = 0;
    $totalPrice = 0;

    //Get Product Price
    $string = file_get_contents("datamodel/Product.json");
    $json_a = json_decode($string,true);
    foreach ($json_a as $key) {
        if($key['name'] == $product){
            $productPrice = $key['price'];
            $productWeight = $key['weight'];
        }
    }
    //Get Shipping Cost
    $string = file_get_contents("datamodel/TariffJne.json");
    $json_a = json_decode($string,true);
    foreach ($json_a as $key) {
        if($key['destination'] == $city){
            $pricePerKg = $key['pricePerKg'];
        }
    }

    $totalWeight = $amount * $productWeight;
    $shippingCost = $pricePerKg * ceil($totalWeight/1000);
    $totalPrice = ($amount * $productPrice) + $shippingCost;


    //get customer id
    $query = "SELECT `id` FROM `Customer` WHERE `phone` = \"{$phoneNumber}\"";
    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));

    $row = mysqli_fetch_row($result);
    $customerId = $row[0];

    mysqli_free_result($result);


    $query = "INSERT INTO `Order`(`id`, `Customer_id`, `address`, `city`, `orderDate`, `quantity`, `totalPrice`, `paid`, `paid_confirm`, `status`)VALUES (NULL,'{$customerId}','{$address}','{$city}','{$date}',{$amount},{$totalPrice},0,0,0)";

    if(mysqli_query($conn,$query) or die(mysqli_error($conn))){
        header('Location:myOrder.php');
        /*echo "data inserted {$product}\n";
        echo ""
        echo "productPrice : ".$productPrice."\n";
        echo "ShippingCost : ".$shippingCost."\n";
        echo "total Price :".$totalPrice;*/
        mysqli_close($conn);
    }else {
        echo "data fail";
        mysqli_close($conn);
    }



?>
