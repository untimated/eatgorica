-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2016 at 05:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eatgoric_order`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `findUserByPhoneNumber`(IN phoneNumber varchar(20))
BEGIN
  SELECT COUNT(*) AS founded FROM `Customer`
  WHERE `Customer`.phone = phoneNumber;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllOrder`()
    NO SQL
BEGIN

SELECT `Order`.`id` AS `Order_id`,`name`,`phone`,`address`,`city`,`orderDate`,`quantity`,`paid`,`paid_confirm`,`status` FROM `Order` JOIN `Customer` ON `Order`.`Customer_id` = `Customer`.`id` ORDER BY `orderDate` DESC;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getCustomer`(IN phoneNumber VARCHAR(20))
BEGIN
	SELECT name,email FROM customer WHERE customer.phone = phoneNumber;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE IF NOT EXISTS `Customer` (
`id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`id`, `phone`, `name`, `email`) VALUES
(1, '0878-8370-1994', 'Michael Herman', 'herman.michael@icloud.com');

-- --------------------------------------------------------

--
-- Table structure for table `Order`
--

CREATE TABLE IF NOT EXISTS `Order` (
`id` int(11) NOT NULL,
  `Customer_id` int(11) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `orderDate` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `totalPrice` int(11) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `paid_confirm` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Order`
--

INSERT INTO `Order` (`id`, `Customer_id`, `address`, `city`, `orderDate`, `quantity`, `totalPrice`, `paid`, `paid_confirm`, `status`) VALUES
(1, 1, 'tes Addres', 'Jakarta', '2015-11-18 09:10:49', 4, NULL, 1, 1, 0),
(2, 1, 'Kedoya agave', 'Jakarta', '2015-11-18 10:43:34', 6, NULL, 1, 0, 0),
(3, 1, 'yyyyy', 'Jakarta', '2015-11-19 04:53:22', 4, NULL, 0, 0, 0),
(4, 1, 'test medan', 'Medan', '2015-11-20 04:39:07', 10, NULL, 0, 0, 0),
(5, 1, 'testing', 'Medan', '2015-11-20 06:26:46', 2, NULL, 0, 0, 0),
(6, 1, 'testJSON', 'Jakarta', '2015-11-24 07:21:57', 4, 0, 0, 0, 0),
(7, 1, 'test JSON 2', 'Jakarta', '2015-11-24 07:27:20', 16, 0, 0, 0, 0),
(8, 1, 'adadada', 'Jakarta', '2015-11-24 07:29:28', 4, 0, 0, 0, 0),
(9, 1, 'SEPULUH', 'Jakarta', '2015-11-24 07:30:37', 10, 0, 0, 0, 0),
(10, 1, 'sebelxas', 'Jakarta', '2015-11-24 07:31:49', 11, 0, 0, 0, 0),
(11, 1, 'CONTOH', 'Jakarta', '2015-11-24 08:08:27', 18, 0, 0, 0, 0),
(12, 1, 'sdfsdfsdf', 'Jakarta', '2015-11-24 08:10:45', 6, 0, 0, 1, 0),
(13, 1, 'teteteteteteeeesssstt', 'Jakarta', '2015-11-24 08:19:12', 5, 175000, 0, 0, 0),
(14, 1, 'JNE', 'Jakarta', '2015-11-24 08:22:45', 56, 2023000, 1, 0, 0),
(15, 1, 'Jalan Kuda Api', 'Jakarta', '2016-02-02 13:04:04', 5, 184000, 1, 1, 0),
(16, 1, 'LOL', 'Medan', '2016-02-02 13:13:06', 1, 62000, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Product`
--

CREATE TABLE IF NOT EXISTS `Product` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Product`
--

INSERT INTO `Product` (`id`, `name`, `weight`, `price`) VALUES
(1, 'Abon Ikan Cakalang', 120, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `ShippingAddress`
--

CREATE TABLE IF NOT EXISTS `ShippingAddress` (
`id` int(11) NOT NULL,
  `Customer_id` int(11) NOT NULL,
  `address` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `TariffJne`
--

CREATE TABLE IF NOT EXISTS `TariffJne` (
`id` int(11) NOT NULL,
  `destination` varchar(45) DEFAULT NULL,
  `pricePerKg` int(11) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TariffJne`
--

INSERT INTO `TariffJne` (`id`, `destination`, `pricePerKg`, `duration`) VALUES
(1, 'Jakarta', 9000, '1-2'),
(2, 'Bandung', 10000, '1-2'),
(3, 'Medan', 27000, '1-2'),
(4, 'Bogor', 8000, '1-2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
 ADD PRIMARY KEY (`id`,`phone`);

--
-- Indexes for table `Order`
--
ALTER TABLE `Order`
 ADD PRIMARY KEY (`id`,`Customer_id`), ADD KEY `fk_Order_Customer1_idx` (`Customer_id`);

--
-- Indexes for table `Product`
--
ALTER TABLE `Product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ShippingAddress`
--
ALTER TABLE `ShippingAddress`
 ADD PRIMARY KEY (`id`,`Customer_id`), ADD KEY `fk_ShippingAddress_Customer1_idx` (`Customer_id`);

--
-- Indexes for table `TariffJne`
--
ALTER TABLE `TariffJne`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Order`
--
ALTER TABLE `Order`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ShippingAddress`
--
ALTER TABLE `ShippingAddress`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `TariffJne`
--
ALTER TABLE `TariffJne`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Order`
--
ALTER TABLE `Order`
ADD CONSTRAINT `fk_Order_Customer1` FOREIGN KEY (`Customer_id`) REFERENCES `Customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ShippingAddress`
--
ALTER TABLE `ShippingAddress`
ADD CONSTRAINT `fk_ShippingAddress_Customer1` FOREIGN KEY (`Customer_id`) REFERENCES `Customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
