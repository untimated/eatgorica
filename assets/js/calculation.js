    function addQuantity() {
        var amount = document.getElementsByName("quantity")[0].value;
        amount = parseInt(amount);
        amount++;
        document.getElementsByName("quantity")[0].value = amount;
    }

    function decreaseQuantity() {
        var amount = document.getElementsByName("quantity")[0].value;
        amount = parseInt(amount);
        if (amount > 0) amount--;
        document.getElementsByName("quantity")[0].value = amount;

    }

    function calculateTariff() {

        $('#panel_info').fadeOut('slow', function () {
            
            var xmlhttpProduct = new XMLHttpRequest();   
            var urlProduct = "product.php";

            //Product Global Variable Initialization
            var productSelected = document.getElementById("choose_product");
            var productName = productSelected.options[productSelected.selectedIndex].value;
            //value Fixed set for client view only - future update using dynamic retrieval
            var productPrice = 0;
            var productWeight = 0;
    

            xmlhttpProduct.onreadystatechange = function(){
                if (xmlhttpProduct.readyState == 4 && xmlhttpProduct.status == 200){
                    var jsonStr = xmlhttpProduct.responseText;
                    console.log(jsonStr);
              
                    var arrProd = JSON.parse(jsonStr);

                    for(var i = 0; i<arrProd.length;i++){
                        if(arrProd[i].id = productName){
                            productName = arrProd[i].name;
                            productPrice = arrProd[i].price;
                            productWeight = arrProd[i].weight;
                            console.log(productName + "," + arrProd[i].id);
                        }else{
                            productName = "Not Found";

                        }
                    }
                    
                }
               

            }



            xmlhttpProduct.open("GET", urlProduct, false);
            xmlhttpProduct.send();

       
            var xmlhttpTariff = new XMLHttpRequest();
            var urlTariff = "tariff.php";

            xmlhttpTariff.onreadystatechange = function () {
                if (xmlhttpTariff.readyState == 4 && xmlhttpTariff.status == 200) {

                    var shipDestination = document.getElementsByName("choose_city")[0].value;
                    var amountItem = document.getElementsByName("quantity")[0].value;

                    
                    var totalWeight = amountItem * productWeight;
                    //from JSON value
                    var costPerKg, totalCost = 0, shippingCost = 0;

                    var str = xmlhttpTariff.responseText;
                    console.log(str);
                    var arr = JSON.parse(str);
                    

                    //Retrieve shipping information : Price and Destination Name
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i].destination == shipDestination) {
                            costPerKg = arr[i].pricePerKg;

                        }
                    }

                    document.getElementById("out_product").innerHTML = productName;
                    document.getElementById("out_amount").innerHTML = amountItem
                    document.getElementById("out_destination").innerHTML = shipDestination;
                    document.getElementById("out_pricePerKg").innerHTML = "Rp. ".concat(numeral(costPerKg).format('0,0').toString());
                    
                    document.getElementById("out_weight").innerHTML = productWeight.toString().concat(" gr x ", amountItem, " = ", totalWeight, " gr");
                    
                    
                    shippingCost = costPerKg * Math.ceil(totalWeight / 1000);
                    document.getElementById("out_shippingCost").innerHTML = "Rp. ".concat(numeral(shippingCost).format('0,0').toString());
                    //total is equal to cost of item purchasing + cost of shipping
                    totalCost = (amountItem * productPrice) + shippingCost;
                    document.getElementById("out_total").innerHTML = "Rp. ".concat(numeral(totalCost).format('0,0').toString());

                    //ajaxReady = false;
                }
            }

            xmlhttpTariff.open("GET", urlTariff, false);
            xmlhttpTariff.send();
            
            $('#panel_info').fadeIn('slow');
        });
    }
