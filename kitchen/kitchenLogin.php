<?php 
    
    session_start(); 

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>gorica</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../assets/css/login.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/css/ionicons.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/jquery-1.6.4.js"></script>

    
    <script type="text/javascript">
         
             
    </script>


    <!-- end smooth scrolling script -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
         < script src = "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js" >
    </script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<?php
    
    if(isset($_POST['adminUserName']) && isset($_POST['adminPassword'])){

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            //echo"PASS";
            $username = $_POST['adminUserName'];
            $password = $_POST['adminPassword'];
            if($username == "joeyjovan@gmail.com" && $password=="zxcvbnm,./")
            {
            
                $_SESSION['adminLoginSuccess'] = "Joey";
                session_write_close();
                header("Location:kitchen.php");
                die();
                

            }else { echo "wrong";}
        }
    }

?>

<div id="headLogin">
    <div class="logo">gorica <sub>~kitchen</sub></div>
    
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <h1 class="text-center login-title">Sign In</h1>
        <div class="account-wall">
            <h2><b>gorica</b></h2>
            <form class="form-signin" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
                <input type="text" class="form-control" placeholder="Username" name="adminUserName" required autofocus/>
                <input type="password" class="form-control" placeholder="Password" name="adminPassword" required/>
                <input class="btn btn-lg btn-warning btn-block" type="submit" value = "Sign In"/>
                
                
                <a href="#" class="pull-right need-help">Need help?</a><span class="clearfix"></span>
            </form>
        </div>
        <a href="../index.php" class="text-center new-account">Return to Gorica Home</a>
    </div>
    <!--/row-->
</div>
<!--/container-->
</div>






    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
</body>

</html>
