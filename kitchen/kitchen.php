<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
session_start();

	if(empty($_SESSION['adminLoginSuccess']))
	{
		header("Location: kitchenLogin.php");
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Gorica Dashboard</title>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<link href="../assets/css/ionicons.min.css" rel="stylesheet">
		<link href="../assets/css/style.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- Important: you'll only need one of the Bootcards CSS files below for every platform -->
		<!-- Bootcards CSS for iOS: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-ios.min.css">
		<!-- Bootcards CSS for Android: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-android.min.css">
		<!-- Bootcards CSS for desktop: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-desktop.css">
		<!-- Bootstrap and Bootcards JS -->
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/css/bootcard/js/bootcards.min.js"></script>
		<!-- sideBar -->
		<script src="../assets/js/mmenu_js/jquery.mmenu.min.js" type="text/javascript"></script>
		<link href="../assets/css/mmenu_css/jquery.mmenu.css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.effects.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.pageshadow.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.themes.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.positioning.css" type="text/css" rel="stylesheet">
		

		<script type="text/javascript">

		$(document).ready(function() {
			$("#menu").mmenu({
				// options
				extensions: ["theme-white","pageshadow"],
				offCanvas: {
					position: "right"
				}
			});

			$('#orderTable > tbody > tr').click(function() {
    			// row was clicked
    			var rowId = $(this).find('td:first').html();
    			document.getElementById('selected_row').value = rowId;
    			$("#kitchenModal").modal('show');
    			//$("#form1").submit()
    			//alert("You click me! ");
			});

			$("#modal_confirm_button").click(function(){
				var row = document.getElementById('selected_row').value;
				if(row != 0) {
					$("#form1").submit();
				}

			})
		});
		</script>
		
	</head>
	<body style="padding-top : 0;">

		<nav id="menu">
			<ul>
				<li class="Selected">

						<ul class="Vertical">
							<li  class="Selected"><a href="kitchen.php">Dashboard</a></li>
							<li><a href="kitchenDashboard.php">Pesanan</a></li>
							<li><a href="kitchenLogin.php">Sign Out</a></li>
						</ul>

				</li>
			</ul>
		</nav>

		<div id="headLogin">
			<div class="logo"><span style="font-size:90%;">gorica <sub>~kitchen</sub></span>  </div>
			<div class="social">
				<a href="#menu"><i><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></i></a>
			</div>
			<p class="welcome">hi, Joey</p>
			<p class="subwelcome">29 October 2015, 5PM</p>
			<div class="container">
				<div class="container">				

						<div class="row">
							<!--List of Order-->
								<div class="panel panel-default bootcards-summary">
								  <div class="panel-heading">
								    <h3 class="panel-title">Dashboard</h3>
								  </div>
								  <div class="panel-body">
								    <div class="row">
								      <div class="col-xs-12 col-sm-12 col-md-6">
								        <a class="bootcards-summary-item" href="kitchenDashboard.php">
								        <span style="font-size:3.5em;" class="glyphicon glyphicon-shopping-cart"></span>
								          <h4>Manage Order<span class="label label-info">
								          <? 
								           // Jumlah Semua Item
								           ?></span></h4>
								        </a>
								      </div>
								      <div class="col-xs-12 col-sm-12 col-md-6">
								        <a class="bootcards-summary-item" href="#">
								          <span style="font-size:3.5em;" class="glyphicon glyphicon-cog"></span>
								          <h4>Product Configuration <span class="label label-info">98</span></h4>
								        </a>
								      </div>
     
								    </div>
								  </div>
								  <div class="panel-footer">
								    <small>gorica 2016</small>
								  </div>
								</div>
						</div>

				</div>
			</div>

			<!--MODAL-->


			<div class="modal fade bs-example-modal-sm" id="kitchenModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">

						<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Modal title</h4>
					      </div>
      					<div class="modal-body">
					        <p>Konfirmasi pembayaran dari Account tersebut ?</p>
					     </div>
						
						<div class="modal-footer">
						    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						    <button id="modal_confirm_button" type="button" class="btn btn-default">Confirm</button>
						 </div>
					</div>
				</div>
			</div>

		</div>
		
		
		<!--/row-->
	</body>
</html>