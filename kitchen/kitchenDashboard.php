<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
session_start();

	if(empty($_SESSION['adminLoginSuccess']))
	{
		header("Location: kitchenLogin.php");
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Gorica Dashboard</title>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<link href="../assets/css/ionicons.min.css" rel="stylesheet">
		<link href="../assets/css/style.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<!-- Important: you'll only need one of the Bootcards CSS files below for every platform -->
		<!-- Bootcards CSS for iOS: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-ios.min.css">
		<!-- Bootcards CSS for Android: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-android.min.css">
		<!-- Bootcards CSS for desktop: -->
		<link rel="stylesheet" href="../assets/css/bootcard/css/bootcards-desktop.css">
		<!-- Bootstrap and Bootcards JS -->
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/css/bootcard/js/bootcards.min.js"></script>
		<!-- sideBar -->
		<script src="../assets/js/mmenu_js/jquery.mmenu.min.js" type="text/javascript"></script>
		<link href="../assets/css/mmenu_css/jquery.mmenu.css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.effects.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.pageshadow.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.themes.css" type="text/css" rel="stylesheet">
		<link href="../assets/css/mmenu_css/extensions/jquery.mmenu.positioning.css" type="text/css" rel="stylesheet">
		

		<script type="text/javascript">

		$(document).ready(function() {
			$("#menu").mmenu({
				// options
				extensions: ["theme-white","pageshadow"],
				offCanvas: {
					position: "right"
				}
			});

			$('#orderTable > tbody > tr').click(function() {
    			// row was clicked
    			var rowId = $(this).find('td:first').html();
    			document.getElementById('selected_row').value = rowId;
    			$("#kitchenModal").modal('show');
    			//$("#form1").submit()
    			//alert("You click me! ");
			});

			$("#modal_confirm_button").click(function(){
				var row = document.getElementById('selected_row').value;
				if(row != 0) {
					$("#form1").submit();
				}

			})
		});
		</script>
		
	</head>
	<body style="padding-top : 0;">

		<nav id="menu">
			<ul>
				<li class="Selected">

						<ul class="Vertical">
							<li><a href="kitchen.php">Dashboard</a></li>
							<li  class="Selected"><a href="kitchenDashboard.php">Pesanan</a></li>
						</ul>

				</li>
			</ul>
		</nav>

		<div id="headLogin">
			<div class="logo"><span style="font-size:90%;">gorica <sub>~kitchen</sub></span>  </div>
			<div class="social">
				<a href="#menu"><i><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></i></a>
			</div>
			<p class="welcome">hi, Joey</p>
			<p class="subwelcome">29 October 2015, 5PM</p>
			<div class="container">
				<div class="container">				
					<div id="g2">
						<div class="row">
							<!--List of Order-->
							<div class="col-sm-12 col-md-12">
								<div class="panel panel-info">
									<div class="panel-heading">
										<h3 class="panel-title">Pesanan Masuk</h3>
									</div>
									<div class="table-responsive table">
										<table id="orderTable" class="table table-hover">
											<thead>
												<th>Order Number</th>
												<th>Name</th>
												<th>Phone Number</th>
												<th>Address</th>
												<th>City</th>
												<th>Order Date</th>
												<th>Quantity</th>
												<th>Paid</th>
												<th>Confirm Payment</th>
												<th>Order Status</th>
											</tr>
										</thead>
										<tbody>

											<?php
											if(isset($_POST['selected_row'])){

												include_once('../connectDB.php');
												$selectedRow = $_POST['selected_row'];

												$query = "UPDATE `Order` set `paid_confirm`=1 WHERE id={$selectedRow}";
												mysqli_query($conn,$query) or die(mysqli_error($conn));

											}
											?>
											
											<form id="form1" action=<?php echo htmlspecialchars($_SERVER["PHP_SELF"])?> method='POST'>
											<input type=hidden id="selected_row" name="selected_row" value=0></input>
											</form>
											
											<?php
											include_once('../connectDB.php');
											$query = "call getAllOrder();";
											$result = mysqli_query($conn,$query) or die(mysqli_error($conn));
											while($row = mysqli_fetch_assoc($result)){
											
											$output = "

											<tr style=\"cursor:pointer;\">
														<td>".$row['Order_id']."</td>
														<td>".$row['name']."</td>
														<td>".$row['phone']."</td>
														<td>".$row['address']."</td>
														<td>".$row['city']."</td>
														<td>".$row['orderDate']."</td>
														<td>".$row['quantity']."</td>
														<td>".($row['paid']==1?"<span class='label label-primary'>paid</span>":"<span class='label label-default'>Unpaid</span>")."</td>
														<td>".($row['paid_confirm']==1?"<span class='label label-info'>Confirmed</span>":"<span class='label label-default'>Unconfirmed</span>")."</td>
														<td>".($row['status']==1?"<span class='label label-sucess'>Done</span>":"<span class='label label-danger'>Inactive</span>")."</td>
											</tr>";
											echo $output;
											}
											?>
										</tbody>
									</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!--MODAL-->


			<div class="modal fade bs-example-modal-sm" id="kitchenModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">

						<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Modal title</h4>
					      </div>
      					<div class="modal-body">
					        <p>Konfirmasi pembayaran dari Account tersebut ?</p>
					     </div>
						
						<div class="modal-footer">
						    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						    <button id="modal_confirm_button" type="button" class="btn btn-default">Confirm</button>
						 </div>
					</div>
				</div>
			</div>

		</div>
		
		
		<!--/row-->
	</body>
</html>